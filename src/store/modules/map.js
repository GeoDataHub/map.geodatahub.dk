import store from "@/store/store";
import Vue from "vue";

export const state = {
  layers: {
    "background map": {
      description: "",
      visible: true
    },
    datasets: {
      description: "",
      visible: true
    },
    "data-density-map": {
      description: "",
      visible: true
    },
    "geojson-search": {
      description: "Draw polygon",
      visible: true
    }
  }
};

export const mutations = {
  SET_LAYER_VISIBILITY(state, newState) {
    //Vue.set(vm.someObject, 'b', 2)
    Vue.set(state.layers[newState.layer], "visible", newState.visible);
  }
};
