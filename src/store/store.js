import Vue from "vue";
import Vuex from "vuex";
import * as auth from "@/store/modules/auth.js";
import * as map from "@/store/modules/map.js";
import { responseToGeoJson } from "@/services/gdhClient.js";

Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    auth, // The current user
    map // State of map components
  },
  store: {
    queryResults: [],
    currentDatasetSelection: null,
    projectedGeometryBounds: null,
    notifyUserMsg: null,
    showUserMsg: null,
    activeAPIComm: null // Is there currectly communication with the API
  },
  mutations: {
    SET_SEARCH_RESULTS(state, searchResults) {
      // Update full array in a reactive way

      if (searchResults["projected_geometry"] instanceof Object) {
        // Single dataset object likely from a dataset lookup. This case
        // is also activated if the user follows a direct link to a dataset.
        const singleDataset = responseToGeoJson([searchResults]);
        Vue.set(state, "queryResults", [singleDataset["geojson"]]);
        Vue.set(state, "projectedGeometryBounds", singleDataset["bounds"]);
      } else if (searchResults["geojson"] instanceof Object) {
        // Wrap a single dataset as a list
        Vue.set(state, "queryResults", [searchResults["geojson"]]);
        Vue.set(state, "projectedGeometryBounds", searchResults["bounds"]);
      } else {
        Vue.set(state, "queryResults", searchResults["geojson"]);
        Vue.set(state, "projectedGeometryBounds", searchResults["bounds"]);
      }
    },
    SET_SEARCH_SELECTION(state, searchSelection) {
      Vue.set(state, "currentDatasetSelection", searchSelection);
    },
    SET_API_COMM_STATE(state, APICommState) {
      // Is there a current communication with the API?
      Vue.set(state, "activeAPIComm", APICommState);
    },
    SET_USER_MESSAGE(state, payload) {
      Vue.set(state, "notifyUserMsg", payload);
      //Vue.set(state, "notifyLevel", payload.level)
      Vue.set(state, "showUserMsg", true);
    },
    CLOSE_USER_MESSAGE(state) {
      Vue.set(state, "showUserMsg", false);
    }
  },
  actions: {
    storeSearch({ commit }, searchResults) {
      commit("SET_SEARCH_RESULTS", searchResults);
      commit("SET_API_COMM_STATE", false);
    },
    storeSelection({ commit }, searchSelection) {
      commit("SET_SEARCH_SELECTION", searchSelection);
      commit("SET_API_COMM_STATE", false);
    },
    notifyUser({ commit }, message) {
      commit("SET_USER_MESSAGE", message);
    }
  },
  getters: {
    datasetToGeoJSON: state => {
      if (state.queryResults) {
        return state.queryResults[0];
      } else {
        // Return empty array if results
        // are empty
        return [];
      }
    },
    datasetBounds: state => {
      if (state.projectedGeometryBounds) {
        return state.projectedGeometryBounds;
      }
    },
    queryResults: state => {
      return state.queryResults;
    },
    searchSelection: state => {
      return state.currentDatasetSelection;
    },
    notifyUserMsg: state => {
      return state.notifyUserMsg;
    },
    currentUser: state => {
      return state.auth.user;
    },
    activeAPIComm: state => {
      return state.activeAPIComm;
    },
    layers: state => {
      return state.map.layers;
    }
  }
});
