// This file contains all information about the
// current user
import axios from "axios";
import gdhClient from "@/services/gdhClient";
import store from "@/store/store";

export const state = {
  user: null,
  loggedIn: false,
  app_id: process.env.VUE_APP_AUTH_APP_ID,
  base_auth_url: process.env.VUE_APP_AUTH_BASE_URL,
  redirect_url: process.env.VUE_APP_AUTH_REDIRECT_URL, // 'http://localhost:8080'
  aws_region: process.env.VUE_APP_AWS_REGION
};

export const mutations = {
  SET_USER_DATA(state, userData) {
    // This function is called everytime the user has to request a new
    // refresh token (i.e. a full login)
    // Store user data locally
    state.user = userData;
    state.loggedIn = true;

    // Store user data in browser storage
    localStorage.setItem("user", JSON.stringify(userData));
  },
  SET_REFRESH_TOKEN_DATA(state, authData) {
    // Create object to store on disk
    state.user = {
      id_token: authData.AuthenticationResult.IdToken,
      access_token: authData.AuthenticationResult.AccessToken,
      refresh_token: state.user.refresh_token, // Reuse the existing token since it will always exist in this case
      expires_in: authData.AuthenticationResult.ExpiresIn,
      token_type: "Bearer"
    };
    state.loggedIn = true;

    // Store user data in browser storage
    localStorage.setItem("user", JSON.stringify(state.user));
  }
};

export const actions = {
  login({ commit }, code) {
    // login to AWS using code flow
    var auth_url = state.base_auth_url + "/oauth2/token";

    // Setup parameters to send to AWS
    const params = new URLSearchParams();
    params.append("grant_type", "authorization_code");
    params.append("client_id", state.app_id);
    params.append("code", code);
    params.append("redirect_uri", state.redirect_url);

    var headers = { "Content-type": "application/x-www-form-urlencoded" };
    return axios
      .post(auth_url, params, {
        headers: headers
      })
      .then(({ data }) => {
        commit("SET_USER_DATA", data);
      });
  },
  logoutUser({ commit }) {
    // Remove all states that would indicate the user is logged in
    localStorage.removeItem("user");
    state.user = null;
    state.loggedIn = false;
  },
  refreshToken({ commit }) {
    // Refresh ID token using the refresh token flow
    var auth_url =
      "https://cognito-idp." + state.aws_region + ".amazonaws.com/";

    var headers = {
      "X-Amz-Target": "AWSCognitoIdentityProviderService.InitiateAuth",
      "Content-Type": "application/x-amz-json-1.1"
    };

    var payload = {
      ClientId: state.app_id,
      AuthFlow: "REFRESH_TOKEN_AUTH",
      AuthParameters: {
        REFRESH_TOKEN: state.user.refresh_token
      }
    };
    return axios
      .post(auth_url, JSON.stringify(payload), {
        headers: headers
      })
      .then(({ data }) => {
        return data;
      })
      .catch(error => {
        var msg = error;
        if (error.data && error.data.has("message")) {
          msg = error.data["message"];
        }
        store.dispatch("notifyUser", {
          msg: "Unable to reauthenticate with error, " + msg,
          level: "error"
        });
        return null;
      });
  }
};
