// vue.config.js file to be place in the root of your repository
// make sure you update `yourProjectName` with the name of your GitLab project
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

module.exports = {
  // Required to use Vuetify - https://vuejs.org/v2/guide/installation.html#Runtime-Compiler-vs-Runtime-only
  runtimeCompiler: true,

  configureWebpack: {
    //plugins: [new BundleAnalyzerPlugin()],
    resolve: {
      // Configure deck.gl to minimize final bundle size
      // https://deck.gl/docs/developer-guide/building-apps
      mainFields: ["esnext", "module", "browser", "main"]
    }
  },

  publicPath: process.env.NODE_ENV === "production" ? "/" : "/"
};
