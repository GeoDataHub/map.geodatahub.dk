# GeoDataHub Web Map

**This repository is part of a larger experiment to build a geoscientific data catalog using modern cloud architectures with a special focus on collecting all data types within a single system to break down silos.**

**The project is stable but not production-ready and no longer maintained in its current form. It's released to inspire others and showcase how to build such a platform. See [getgeodata](getgeodata.com) for more.**

**If you have technical questions about the design (not the installation or setup) you are welcome to contact hello@getgeodata.com**

This project is the frontend for the [GeoDataHub RESTful API](https://gitlab.com/GeoDataHub/api.geodatahub.dk). Using the map users are allowed to discover and modify geoscientific datasets.

The map uses Uber's [deck.gl](deck.gl) project and [Vue.js](https://vuejs.org/).

See a live demo of the project [here](https://www.map.getgeodata.com).

See the full list of projects within the GeoDataHub project on the [Gitlab project](https://gitlab.com/GeoDataHub).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
