import axios from "axios";
import store from "@/store/store";

const gdhClient = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL
});

function handleResponseError(error) {
  // Stop showing API communication on error
  store.commit("SET_API_COMM_STATE", false);

  const errorResponse = error.response;
  // If the response was empty the OPTIONS call likely failed with a 401. This should
  // force a token refresh
  /*
  if (errorResponse === undefined || errorResponse.status == 401) {
    // If the API returns an empty error then likely something really bad happens. Likely
    // an network error due to a failed refresh token flow. In this case try to logout the
    // user and see if a fresh token helps.
    store.dispatch("logoutUser");
    return null;

  } else
  */
  if (errorResponse.status == 404) {
    store.dispatch("notifyUser", {
      msg:
        "The requested page does not exist! Please contact support@geodatahub.dk",
      level: "error"
    });
  } else {
    var msg = errorResponse.data;
    if (errorResponse.data.hasOwnProperty("error")) {
      // API responses
      msg = errorResponse.data["error"];
    } else if (errorResponse.data.hasOwnProperty("message")) {
      // AWS responses
      msg = errorResponse.data["message"];
    }

    store.dispatch("notifyUser", {
      msg: msg,
      level: "error"
    });

    // Stop error here
    return null;
  }
}

// Set the ID token as a header on each request. If the token is invalid the user isn't logged
// in and the header is deleted, if it exists.
function setToken(token) {
  if (token) {
    gdhClient.defaults.headers.common["Authorization"] = token;
  } else {
    // User submits an anonymous request
    delete axios.defaults.headers.common["Authorization"];
  }
}

function isTokenExpiredError(errorResponse) {
  // Logic to determine if the error is due to JWT token expired returns a boolean value
  if (errorResponse.data.message === undefined) {
    // If the error reponse doesn't exist then this is not a problem with the token
    return true;
  }
  const errorMsg = errorResponse.data.message;
  if (errorMsg.localeCompare("The incoming token has expired") == 0) {
    // Token is timedout - needs a refresh
    return true;
  }

  // Do not refresh token
  return false;
}

export function responseToGeoJson(resp) {
  // Convert a dataset object to only GeoJSON for
  // displaying on the map
  var geojsonDatasets = [];
  if (resp.length <= 0) {
    store.dispatch("notifyUser", { msg: "No results found", level: "warning" });
  }

  let minLat = 90;
  let maxLat = -90;
  let minLng = 180;
  let maxLng = -180;

  for (let i = 0; i < resp.length; i++) {
    // Flatten the array multiple to ensure the format is [lng1, lat1, lng2, lat2, lng3, lat3, ...]
    const flatGeometry = resp[i].projected_geometry.coordinates.flat().flat();

    for (let j = 0; j < flatGeometry.length; j++) {
      if (j % 2) {
        minLat = Math.min(minLat, flatGeometry[j]);
        maxLat = Math.max(maxLat, flatGeometry[j]);
      } else {
        minLng = Math.min(minLng, flatGeometry[j]);
        maxLng = Math.max(maxLng, flatGeometry[j]);
      }
    }

    // TODO: Can we skip this step and save memory and computations?
    var obj = {
      type: "Feature",
      geometry: {
        type: resp[i].projected_geometry.type,
        coordinates: resp[i].projected_geometry.coordinates
      },
      properties: {
        identifier: resp[i].identifier,
        datatype: resp[i].datatype
      }
    };
    geojsonDatasets.push(obj);
  }
  var geojsonCollection = {
    type: "FeatureCollection",
    features: geojsonDatasets
  };
  var bbox = [
    [minLng, minLat],
    [maxLng, maxLat]
  ];
  return { geojson: geojsonCollection, bounds: bbox };
}

function ping() {
  return gdhClient.get("/ping").catch(error => handleResponseError(error));
}

function searchDatasets(query, includePublicData) {
  return checkToken().then(token => {
    setToken(token);
    store.commit("SET_API_COMM_STATE", true);

    // Get a dataset from the backend with a given ID
    const params = {
      params: {
        q: query,
        public: includePublicData
      }
    };

    return gdhClient
      .get("/datasets", params)
      .then(response => {
        return responseToGeoJson(response.data);
      })
      .catch(error => {
        return handleResponseError(error);
      });
  });
}

function getDataset(identifier) {
  return checkToken().then(token => {
    setToken(token);
    store.commit("SET_API_COMM_STATE", true);

    return gdhClient.get("/datasets".concat("/", identifier)).then(response => {
      return response.data;
    });
    /*
      .catch(error => {
        return handleResponseError(error);
      });
      */
  });
}

function getSchemaAutocompleteOptions(schemaPath, schemaParam) {
  // Get the range of values a single schema parameter can hold
  // in the database. For string parameters the returned response
  // is a unique list of values. For numeric paramters the histogram
  // of possible values is returned.
  return checkToken().then(token => {
    setToken(token);

    store.commit("SET_API_COMM_STATE", true);
    return gdhClient
      .get("/schemas/autocomplete", {
        params: {
          schema: schemaPath,
          key: schemaParam
        }
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return handleResponseError(error);
      });
  });
}

function getUser(userID) {
  // Get information about the current user
  return checkToken().then(token => {
    setToken(token);
    store.commit("SET_API_COMM_STATE", true);

    if (!userID) {
      // Default to show current user
      userID = "";
    }

    return gdhClient
      .get("/users".concat("/", userID))
      .then(response => {
        store.commit("SET_API_COMM_STATE", false);
        return response.data;
      })
      .catch(error => {
        return handleResponseError(error);
      });
  });
}

function getUserOrganizations(userID) {
  // Get information about the current user
  return checkToken().then(token => {
    setToken(token);

    store.commit("SET_API_COMM_STATE", true);
    return gdhClient
      .get("/users/" + userID + "/organizations")
      .then(response => {
        store.commit("SET_API_COMM_STATE", false);
        return response.data;
      })
      .catch(error => {
        return handleResponseError(error);
      });
  });
}

function getUsersDataHeatmap() {
  // Get information about the current user
  return checkToken().then(token => {
    setToken(token);

    store.commit("SET_API_COMM_STATE", true);
    return gdhClient
      .get("/datasets-heatmap")
      .then(response => {
        store.commit("SET_API_COMM_STATE", false);
        return response.data;
      })
      .catch(error => {
        // TODO: Errorreponse is undefined
        return handleResponseError(error);
      });
  });
}

function updatePublicState(datasetID, publicize) {
  return checkToken().then(token => {
    setToken(token);

    // Update the dataset between public and private
    if (publicize) {
      // Make dataset public
      return gdhClient
        .post("/datasets/" + datasetID + "/publicize")
        .then(response => {
          return true;
        })
        .catch(error => {
          handleResponseError(error);
          return false;
        });
    } else {
    }
    {
      // Make dataset private
      return gdhClient
        .delete("/datasets/" + datasetID + "/publicize")
        .then(response => {
          return true;
        })
        .catch(error => {
          handleResponseError(error);
          return false;
        });
    }
  });
}

function genericCall(uri, method) {
  return checkToken().then(token => {
    setToken(token);

    store.commit("SET_API_COMM_STATE", true);
    var resp = undefined;
    var lowMethod = method.toLowerCase();

    if (lowMethod == "get") {
      resp = gdhClient.get(uri);
    } else if (lowMethod == "delete") {
      resp = gdhClient.delete(uri);
    } else if (lowMethod == "post") {
      resp = gdhClient.post(uri);
    } else {
      return undefined;
    }

    return resp
      .then(response => {
        return response;
      })
      .catch(error => {
        return handleResponseError(error);
      });
  });
}

// Check ID token expiration before sending request
function checkToken() {
  var token = null;
  // Attach GDH token to request if user is logged in
  if (store.getters.currentUser !== null) {
    token = store.getters.currentUser.id_token;
  }

  // Check if token is close to expire
  if (token) {
    let expires_at = JSON.parse(atob(token.split(".")[1]))["exp"];
    let token_lifetime_sec = expires_at - Date.now() / 1000;

    // Force a token refresh if the current has expired
    if (token_lifetime_sec <= 0) {
      token = store
        .dispatch("refreshToken")
        .then(data => {
          if (data === null) {
            // Refresh flow was not success full
            store.dispatch("logoutUser");
            return null;
          } else {
            // Send new token to store
            store.commit("SET_REFRESH_TOKEN_DATA", data);

            // Update request with new token
            return data.AuthenticationResult.IdToken;
          }
        })

        // In case the request was unable to get a fresh token the users refresh
        // token has likely expired. Force the user to login again to get a fresh token
        .catch(error => {
          store.dispatch("logoutUser");
          return null;
        });
    }
  }
  return Promise.resolve(token);
}

export default {
  ping,
  searchDatasets,
  getDataset,
  getSchemaAutocompleteOptions,
  getUser,
  getUserOrganizations,
  genericCall,
  updatePublicState,
  getUsersDataHeatmap
};
