import Vue from "vue";
import VueRouter from "vue-router";
import Login from "./views/Login.vue";
import Dataset from "./views/Dataset.vue";
import DeckMap from "./views/DeckMap.vue";
import PageNotFound from "./views/PageNotFound.vue";
import App from "./App.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: App
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/dataset/:id",
      name: "dataset",
      component: Dataset
    },
    {
      path: "*",
      component: PageNotFound
    }
  ]
});
