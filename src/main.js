import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  router: router,
  store,

  created() {
    // Get past user from local storage so they do not
    // have to login again
    const userString = localStorage.getItem("user");

    if (userString) {
      const userData = JSON.parse(userString);
      this.$store.commit("SET_USER_DATA", userData);
    }
  },
  vuetify,
  render: h => h("router-view")
});
