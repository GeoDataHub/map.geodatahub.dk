import axios from "axios";
import store from "@/store/store";

const schemaClient = axios.create({
  baseURL: "https://schemas.getgeodata.com"
});

export default {
  call: {
    getSchemas() {
      return schemaClient.get("/full_schemas.json").then(response => {
        return response.data;
      });
    }
  }
};
